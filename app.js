//imports
const express = require('express')
const app = express()
const etudiants = require('./etudiants.json')




app.use(express.json())



//configure INFO routes

// get /
app.get('/', function (req, res) {
  res.send('info.html')
})

// GET /info
app.get('/', function (req, res) {
  res.send('info.html')
})



// POST /info
app.post('/info', function (req, res) {
  res.send('get a post request')
})


//configure etudiant routes
// GET //etudiants
app.get('/etudiants', (req,res) => {
    res.status(200).json(etudiants)
})

// GET /etudiants/:id
app.get('/etudiants/:id', (req,res) => {
    const id = parseInt(req.params.id)
    const etudiant = etudiants.find(etudiant=> etudiant.id === id)
    res.status(200).json(etudiant|| null)
})

// POST /etudiants
app.post('/etudiants', (req,res) => {
    etudiants.push(req.body)
    res.status(200).json(etudiants)
})

// PUT /etudiants/:id
app.put('/etudiants/:id', (req,res) => {
    const id = parseInt(req.params.id)
    let etudiant = etudiants.find(etudiant=> etudiant.id === id)
    etudiant.firstname =req.body.firstname,
    etudiant.lastname =req.body.lastname,
    etudiant.email =req.body.email,
    res.status(200).json(etudiant)
})

// DELETE /etudiants/:id
app.delete('/etudiants/:id', (req,res) => {
    const id = parseInt(req.params.id)
    let etudiant = etudiants.find(etudiant => etudiant.id === id)
    etudiants.splice(etudiants.indexOf(etudiant),1)
    res.status(200).json(etudiants)
})



//lunch server
app.listen(8080, () => {
    console.log("I am ready")
})
